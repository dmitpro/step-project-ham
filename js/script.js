let countClickWorkBtn = 0;
let category;
let indexImg = 12;

$('.services-tabs-title').click(function () {
  $('.services-tabs-title').removeClass('active');
  $('.services-tabs-title-caret').removeClass('visible');
  $('.services-tabs-content').removeClass('open');
  $(this).addClass('active');
  $('i', this).addClass('visible');
  $('.services-tabs-content').eq($('.services-tabs-title').index(this)).addClass('open');
});

$('button').mousedown(function() {
  $(this).animate({'top':'2px'}, 200);
});

$('button').mouseup(function() {
 	$(this).animate({'top':'-2px'}, 200);
});

$('.work-menu-item').click(function () {
  category = this.dataset.menuCategory;
  $('.work-menu-item').removeClass('select');
  $(this).addClass('select');

  if (category !== 'All') {
    $('.work-gallery-img.show').css('display', 'none');
    $(`.work-gallery-img.show[data-category='${category}']`).css('display', 'block');
  } else {
    $('.work-gallery-img').filter(function(index) {
      return index < indexImg
    })
        .css('display', 'block');}
});

$('#work-btn').click(function () {
  $(this).prev().css('display', 'block');

  if (countClickWorkBtn < 2) {
    countClickWorkBtn++;
    indexImg += 12;
  }

  if (countClickWorkBtn === 2) this.style.visibility = 'hidden';
  galleryLoad (indexImg);
});

const galleryLoad = function () {
  category = $('.work-menu-item.select')[0].dataset.menuCategory;
  setTimeout(function () {
    $('.loader-wrapp').css('display', 'none');
    $('.work-gallery-img').each(function (index) {
      let currentImg = $('.work-gallery-img').eq(index);

      if (index < indexImg &&
          (category === 'All' || currentImg.data('category') === category )) {
            currentImg.addClass('show');
      }
    })
  }, 2000)
};

$(function(){
  $('.feedback-wrapper').supergallery({
    selectors:{
      main: '.feedback-main',
      thumb: '.feedback-carousel',
      nextBtn: '.nextBtn',
      prevBtn: '.prevBtn',
    },
    animation:{
      type: 'fade',
      duration: 400,
      easing: 'swing'
    },
  });
});

$(document).ready(function() { 
  let $galleryGrid = $('.gallery-grid').masonry({
    itemSelector: '.gallery-item',
    fitWidth: true,
    gutter: 18,
  });

  $('#gallery-btn').click(function () {
    $(this).prev().css('display', 'block');
    setTimeout(function () {
      $('.loader-wrapp').css('display', 'none');
      $('.gallery-item.hidden').removeClass('hidden');
      $galleryGrid.masonry();
    }, 2000);
    this.style.visibility = 'hidden';
  });
});
